using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject menu;
    [SerializeField] GameObject game;
    [SerializeField] GameObject player;
    [SerializeField] Bot bot;
    [SerializeField] List<Sprite> counters;
    [SerializeField] SpriteRenderer timer;
    [SerializeField] Sprite rock;
    [SerializeField] Sprite paper;
    [SerializeField] Sprite scissors;
    float roundTime = 3;
    int playerPoints = 0;

    public void OnPlayButtonClick() 
    {
        menu.SetActive(false);
        game.SetActive(true);
    }
    public void Update()
    {
        if (roundTime == 10)
        {
            return;
        } 
        if (roundTime <= 0)
        {
            roundTime = 10;
            bot.DrawBotSprite();
            StartCoroutine(WaitForNextRound());
        }
        else 
        {
            roundTime -= Time.deltaTime;
            if (Math.Ceiling(roundTime) == 3)
            {
                timer.sprite = counters[2];
            }
            else if (Math.Ceiling(roundTime) == 2)
            {
                timer.sprite = counters[1];
            }
            else if (Math.Ceiling(roundTime) == 1)
            {
                timer.sprite = counters[0];
            }
            else 
            {
                timer.sprite = null;
            }
        }
    }
    public void OnMenuButtonClick() 
    {
        menu.SetActive(true);
        game.SetActive(false);
        roundTime = 3;
        playerPoints = 0;
        bot.point = 0;
        player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
        bot.ResetBotSprite();
        player.transform.GetChild(1).gameObject.SetActive(true);
        player.transform.GetChild(2).gameObject.SetActive(true);
        player.transform.GetChild(3).gameObject.SetActive(true);
        player.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "0";
        bot.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "0";
    } 
    public void OnRockButtonClick() 
    {
        player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = player.transform.GetChild(1).GetComponent<Button>().image.sprite;
        HideButtons();
    }
    public void OnPaperButtonClick()
    {
        player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = player.transform.GetChild(2).GetComponent<Button>().image.sprite;
        HideButtons();
    }
    public void OnScissorsButtonClick()
    {
        player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = player.transform.GetChild(3).GetComponent<Button>().image.sprite;
        HideButtons();
    }
    public void HideButtons() 
    {
        timer.sprite = null;
        player.transform.GetChild(1).gameObject.SetActive(false);
        player.transform.GetChild(2).gameObject.SetActive(false);
        player.transform.GetChild(3).gameObject.SetActive(false);
        roundTime = 0;
    }
    public void StartNextRound() 
    {
        player.transform.GetChild(1).gameObject.SetActive(true);
        player.transform.GetChild(2).gameObject.SetActive(true);
        player.transform.GetChild(3).gameObject.SetActive(true);
        roundTime = 3;
    }
    IEnumerator WaitForNextRound() 
    {
        yield return new WaitForSeconds(1f);
        FindWinner();
        player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = null;
        bot.ResetBotSprite();
        StartNextRound();
    }
    public void FindWinner() 
    {
        if (player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite)
        {
            return;
        }
        else
        {

            if (player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == rock)
            {
                if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == paper)
                {
                    bot.point++;
                    bot.AddPointToBot();
                }
                else if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == scissors)
                {
                    playerPoints++;
                    player.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = playerPoints.ToString();

                }
            }
            else if (player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == paper)
            {
                if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == rock)
                {
                    playerPoints++;
                    player.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = playerPoints.ToString();
                }
                else if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == scissors)
                {
                    bot.point++;
                    bot.AddPointToBot();
                }

            }
            else if (player.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == scissors)
            {
                if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == rock)
                {
                    bot.point++;
                    bot.AddPointToBot();
                }
                else if (bot.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite == paper)
                {
                    playerPoints++;
                    player.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = playerPoints.ToString();
                }

            }
            else
            {
                bot.point++;
                bot.AddPointToBot();
            }
        }
    }
    
}
